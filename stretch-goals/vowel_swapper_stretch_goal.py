import re


def vowel_swapper(string):
    char_list = []
    chars_to_swap = {
        "a": "4",
        "A": "4",
        "e": "3",
        "E": "3",
        "i": "!",
        "I": "!",
        "o": "ooo",
        "O": "000",
        "u": "|_|",
        "U": "|_|",
    }

    occurrence = {"a": 0, "e": 0, "i": 0, "o": 0, "u": 0}
    split_str = re.split("", string.lower())

    for chars in split_str:
        for char in chars:
            if char in chars_to_swap:
                occurrence[char] += 1
                if occurrence[char] == 2:
                    char_list.append(chars_to_swap[char])
                else:
                    char_list.append(char)
            else:
                char_list.append(char)

    if string[0].istitle():
        modified_string = "".join(char_list)
        corrected_string = modified_string.title()
    else:
        corrected_string = "".join(char_list)

    return corrected_string


print(
    vowel_swapper("aAa eEe iIi oOo uUu")
)  # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World"))  # Should print "Hello Wooorld" to the console
print(
    vowel_swapper("Everything's Available")
)  # Should print "Ev3rything's Av/\!lable" to the console
