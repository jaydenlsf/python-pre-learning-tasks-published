def factors(number):
    denominators = range(2, number)
    fac = []
    for num in denominators:
        if number % num == 0:
            fac.append(num)
    return fac


print(factors(15))  # Should print [3, 5] to the console
print(factors(12))  # Should print [2, 3, 4, 6] to the console
print(factors(13))  # Should print “[]” (an empty list) to the console
