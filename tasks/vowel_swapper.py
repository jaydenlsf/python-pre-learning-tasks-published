import re


def vowel_swapper(string):
    char_list = []
    chars_to_swap = {
        "a": "4",
        "A": "4",
        "e": "3",
        "E": "3",
        "i": "!",
        "I": "!",
        "o": "ooo",
        "O": "000",
        "u": "|_|",
        "U": "|_|",
    }

    split_str = re.split("", string)
    for chars in split_str:
        for char in chars:
            if char in chars_to_swap:
                char_list.append(chars_to_swap[char])
            else:
                char_list.append(char)
    return "".join(char_list)


print(
    vowel_swapper("aA eE iI oO uU")
)  # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World"))  # Should print "H3llooo Wooorld" to the console
print(
    vowel_swapper("Everything's Available")
)  # Should print "3v3ryth!ng's 4v4!l4bl3" to the console
